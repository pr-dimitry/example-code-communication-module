<?php


namespace App\Libraries\Communication\Facades;

use App\Libraries\Communication\Contracts\EventCommunicationDTOInterface;
use Illuminate\Support\Facades\Facade;

/**
 * Class Communication
 *
 * Source class: App\Libraries\Communication\CommunicationClass
 *
 * @method static after(string $className, string $trigger, EventCommunicationDTOInterface $DTO)
 * @method static sendScheduled()
 *
 * @package App\Libraries\Communication\Facades
 */
class Communication extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'communication';
    }
}
