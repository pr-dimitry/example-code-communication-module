<?php


namespace App\Libraries\Communication;

use App\Libraries\Communication\Contracts\EventCommunicationDTOInterface;
use App\Libraries\Communication\EventCommunication\EventCommunication;
use App\Libraries\Communication\Jobs\SendCommunication;
use App\Libraries\Communication\Services\CommunicationService;

class CommunicationClass
{
    const TEMPLATE_PATH = __DIR__ . DIRECTORY_SEPARATOR . 'templates';

    const TYPE_RAW  = 'raw';
    const TYPE_HTML = 'html';

    const CHANNEL_EMAIL     = 'email';
    const CHANNEL_SMS       = 'sms';
    const CHANNEL_VIBER     = 'viber';
    const CHANNEL_PUSHER    = 'pusher';

    /**
     * Send notification after model event
     *
     * @param string $className model class
     * @param string $trigger
     * @param EventCommunicationDTOInterface $DTO
     * @throws \ErrorException
     */
    public static function after(string $className, string $trigger, EventCommunicationDTOInterface $DTO)
    {
        (new EventCommunication($className, $trigger, $DTO))->run();
    }

    /**
     * This is an communication:send-scheduled command handler
     *
     * @return int
     * @throws \Exception
     */
    public static function sendScheduled()
    {
        $communications = (new CommunicationService())->getScheduled(240);
        foreach ($communications as $communication) {
            $recipients = $communication->getAllRecipients();
            foreach ($recipients as $recipient) {
                SendCommunication::dispatch($communication, $recipient);
            }
        }
        return $communications->count();
    }

    public function custom()
    {

    }
}
