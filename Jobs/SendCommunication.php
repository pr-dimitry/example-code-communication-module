<?php

namespace App\Libraries\Communication\Jobs;

use App\Libraries\Communication\Contracts\CommunicationRecipientInterface;
use App\Libraries\Communication\Sender;
use App\Libraries\Communication\Models\Communication;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Log;

/**
 * Class SendCommunication
 * @package App\Jobs
 */
class SendCommunication implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $communication;
    protected $recipient;

    /**
     * SendCommunication constructor.
     * @param Communication $communication
     * @param CommunicationRecipientInterface $recipient
     */
    public function __construct(Communication $communication, CommunicationRecipientInterface $recipient)
    {
        $this->communication = $communication;
        $this->recipient = $recipient;
    }

    /**
     * Execute the job.
     * @throws \Exception
     */
    public function handle()
    {
        Log::info(__('communication.messages.sending_communication', [
            'id' => $this->communication->id,
            'channel' => $this->communication->channel,
            'recipient' => get_class($this->recipient) . ': ' . $this->recipient->id,
        ]));
        Sender::sendCommunication($this->communication, $this->recipient);
    }
}
