<?php


namespace App\Libraries\Communication\DTO;


use App\Contracts\DTOInterface;
use App\Traits\DTOTrait;

class EmailChannelDTO implements DTOInterface
{
    use DTOTrait;

    protected $from_name;
    protected $from_email;
    protected $to;
    protected $subject;
    protected $body;


    /*  getters  */
    /**
     * @return string
     */
    public function getFromName() : string
    {
        return $this->from_name;
    }

    /**
     * @return string
     */
    public function getFromEmail() : string
    {
        return $this->from_email;
    }

    /**
     * @return string
     */
    public function getTo() : string
    {
        return $this->to;
    }

    /**
     * @return string
     */
    public function getSubject() : string
    {
        return $this->subject;
    }

    /**
     * @return string
     */
    public function getBody() : string
    {
        return $this->body;
    }

    /*  end getters  */


    /*  setters  */

    /**
     * @param string $fromName
     */
    public function setFromName(string $fromName)
    {
        $this->from_name = $fromName;
    }

    /**
     * @param string $fromEmail
     */
    public function setFromEmail(string $fromEmail)
    {
        $this->from_email = $fromEmail;
    }

    /**
     * @param string $toEmail
     */
    public function setTo(string $toEmail)
    {
        $this->to = $toEmail;
    }

    /**
     * @param string $subject
     */
    public function setSubject(string $subject)
    {
        $this->subject = $subject;
    }

    /**
     * @param string $body
     */
    public function setBody(string $body)
    {
        $this->body = $body;
    }

    /*  end setters  */

}
