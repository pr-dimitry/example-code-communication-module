<?php


namespace App\Libraries\Communication\DTO;


use App\Contracts\DTOInterface;
use App\Libraries\Communication\Contracts\CommunicationRecipientInterface;
use App\Libraries\Communication\Models\Communication;

class ChannelDTOFactory
{

    /**
     * @param Communication $communication
     * @param CommunicationRecipientInterface $recipient
     * @return DTOInterface
     */
    public static function make(Communication $communication, CommunicationRecipientInterface $recipient) : DTOInterface
    {
        $methodName = 'make' . ucfirst($communication->channel) . 'Dto';
        return self::$methodName($communication, $recipient);
    }

    /**
     * @param Communication $communication
     * @param CommunicationRecipientInterface $recipient
     * @return EmailChannelDTO
     */
    protected static function makeEmailDto(Communication $communication, CommunicationRecipientInterface $recipient) : EmailChannelDTO
    {
        return EmailChannelDTO::make([
            'fromName' => config('mail.from.name'),
            'fromEmail' => config('mail.from.address'),
            'to' => $recipient->getToEmail(),
            'subject' => $communication->data['heading'],
            'body' => $communication->data['text'],
        ]);
    }

    /**
     * @param Communication $communication
     * @param CommunicationRecipientInterface $recipient
     * @return SmsChannelDTO
     */
    protected static function makeSmsDto(Communication $communication, CommunicationRecipientInterface $recipient) : SmsChannelDTO
    {
        return SmsChannelDTO::make([
            'phoneNumber' => phone($recipient->getPhoneNumber(), config('sms.country')),
            'text' => $communication->data['text'],
        ]);
    }

}
