<?php


namespace App\Libraries\Communication\DTO;


use App\Libraries\Communication\Contracts\EventCommunicationDTOInterface;
use App\Traits\DTOTrait;

class EventCommunicationDTO implements EventCommunicationDTOInterface
{
    use DTOTrait;

    protected $modelId;
    protected $userId;


    /*  getters  */

    /**
     * @return mixed
     */
    public function getModelId()
    {
        return $this->modelId;
    }

    /**
     * @return mixed
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /*  end getters  */


    /*  setters  */

    /**
     * @param int $modelId
     */
    public function setModelId(int $modelId)
    {
        $this->modelId = $modelId;
    }

    /**
     * @param int $userId
     */
    public function setUserId(int $userId)
    {
        $this->userId = $userId;
    }

    /*  end setters  */

}
