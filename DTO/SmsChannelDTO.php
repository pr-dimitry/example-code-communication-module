<?php


namespace App\Libraries\Communication\DTO;


use App\Contracts\DTOInterface;
use App\Traits\DTOTrait;

class SmsChannelDTO implements DTOInterface
{
    use DTOTrait;

    protected $phone_number;
    protected $phone_numbers;
    protected $text;

    /*  getters  */

    /**
     * @return string
     */
    public function getPhoneNumber() : string
    {
        return $this->phone_number;
    }

    /**
     * @return string
     */
    public function getPhoneNumbers() : string
    {
        return $this->phone_numbers;
    }

    /**
     * @return string
     */
    public function getText() : string
    {
        return $this->text;
    }

    /*  end getters  */


    /*  setters  */

    /**
     * @param string $phoneNumber
     */
    public function setPhoneNumber(string $phoneNumber)
    {
        $this->phone_number = $phoneNumber;
    }

    /**
     * @param string $phoneNumbers, e.g. '+380XXXXXXXX1,+380XXXXXXXX2,+380XXXXXXXX3'
     */
    public function setPhoneNumbers(string $phoneNumbers)
    {
        $this->phone_numbers = $phoneNumbers;
    }

    /**
     * @param string $text
     */
    public function setText(string $text)
    {
        $this->text = $text;
    }

    /*  end setters  */

}
