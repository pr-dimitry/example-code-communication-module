<?php

namespace App\Libraries\Communication\Contracts;

use App\Contracts\DTOInterface;

interface EventCommunicationDTOInterface extends DTOInterface
{
    /**
     * @return mixed
     */
    public function getModelId();

    /**
     * @return mixed
     */
    public function getUserId();
}
