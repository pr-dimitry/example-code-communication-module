<?php

namespace App\Libraries\Communication\Contracts;

interface CommunicationRecipientInterface
{
    /**
     * @return mixed
     */
    public function communications();

    /**
     * @return mixed
     */
    public function getToEmail();

    /**
     * @return mixed
     */
    public function getPhoneNumber();
}
