<?php

namespace App\Libraries\Communication\Contracts;

interface CommunicationModelInterface
{
    /**
     * @param string $notification
     * @return mixed
     */
    public function getCommunicationRecipients(string $notification);

}
