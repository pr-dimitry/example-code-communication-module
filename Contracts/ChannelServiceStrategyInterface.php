<?php

namespace App\Libraries\Communication\Contracts;

use App\Libraries\Communication\Models\Communication;

interface ChannelServiceStrategyInterface
{

    /**
     * ChannelServiceStrategyInterface constructor.
     * @param Communication $communication
     * @param $DTO
     */
    public function __construct(Communication $communication, $DTO);

    /**
     * @param array|null $config
     * @return mixed
     */
    public function send(array $config = null);

}
