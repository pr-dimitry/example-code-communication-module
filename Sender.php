<?php


namespace App\Libraries\Communication;


use App\Contracts\DTOInterface;
use App\Libraries\Communication\Contracts\ChannelServiceStrategyInterface;
use App\Libraries\Communication\Contracts\CommunicationRecipientInterface;
use App\Libraries\Communication\DTO\ChannelDTOFactory;
use App\Libraries\Communication\Models\Communication;

class Sender
{

    /**
     * @param Communication $communication
     * @param CommunicationRecipientInterface $recipient
     * @throws \Exception
     */
    public static function sendCommunication(Communication $communication, CommunicationRecipientInterface $recipient)
    {

        // prepare necessary data for notification
        $DTO = ChannelDTOFactory::make($communication, $recipient);

        // get notification service based on channel and send
        (self::makeChannelService($communication, $DTO))->send();
    }

    /**
     * @param string $channel
     * @param DTOInterface $DTO
     * @return ChannelServiceStrategyInterface
     */
    protected static function makeChannelService(Communication $communication, DTOInterface $DTO) : ChannelServiceStrategyInterface
    {
        $className = __NAMESPACE__ . '\\ChannelsServices\\' . ucfirst($communication->channel);
        return new $className($communication, $DTO);
    }

}
