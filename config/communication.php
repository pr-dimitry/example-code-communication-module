<?php

use App\Libraries\Communication\CommunicationClass;
use App\Models\Record;
use App\Libraries\Communication\EventCommunication\RecordEventCommunication;

return [

    'event_communication' => [
        Record::class => [
            'class' => RecordEventCommunication::class,
            'events' => [
                Record::EVENT_ONLINE_CREATED => [
                    'name' => Record::EVENT_ONLINE_CREATED,
                    'notifications' => Record::NOTIFICATIONS[Record::EVENT_ONLINE_CREATED],
                ],
            ],
        ],
    ],

    'channels' => [
        CommunicationClass::CHANNEL_EMAIL => [
            'type' => CommunicationClass::TYPE_HTML,
        ],
        CommunicationClass::CHANNEL_SMS=> [
            'type' => CommunicationClass::TYPE_RAW,
        ],
        CommunicationClass::CHANNEL_VIBER=> [
            'type' => CommunicationClass::TYPE_RAW,
        ],
        CommunicationClass::CHANNEL_PUSHER=> [
            'type' => CommunicationClass::TYPE_RAW,
        ],
    ]

];
