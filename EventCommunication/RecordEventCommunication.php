<?php


namespace App\Libraries\Communication\EventCommunication;

use App\Libraries\Communication\Contracts\CommunicationRecipientInterface;
use App\Libraries\Communication\helpers\CommunicationDate;

class RecordEventCommunication extends BaseEventCommunication
{


    public function record_online_created_communication()
    {

        // create communications
        // @TODO maybe move this code to the base class

        // all notifications bound to this event handler
        $notifications = $this->config['events'][$this->trigger]['notifications'];

        foreach ($notifications as $notification => $notificationConfig) {

            // if scheduled - calculate and attach schedule datetime
            if ($notificationConfig['scheduled']) {
                CommunicationDate::setScheduledDate($notificationConfig, $this->model);
            }

            // request available channels to send, e.g. sms: 1, email: 1 ...
            $channelsSettings = array_key_exists($notification, $this->settings)
                ? $this->settings[$notification]['channels']
                : [];

            // foreach existing channel, see config: config/communication.php
            foreach ($this->channels as $channel => $channelConfig) {

                // check if sending using this channel is allowed in settings
                if (!$this->channelAvailable($channelsSettings, $channel)) {
                    continue;
                }

                $message = $this->communication_service->getMessageData($channelConfig['type'], $notification);
                $recipients = $this->model ? $this->model->getCommunicationRecipients($notification) : false;

                if ($recipients) {

                    $newCommunication = $this->communication_service->createCommunication($message, $channel, $notificationConfig);

                    // each message could be sent to group of recipients, attaching recipients to communication
                    foreach ($recipients as $recipient) {
                        /**
                         * @var CommunicationRecipientInterface $recipient
                         */
                        $recipient->communications()->attach($newCommunication);
                    }

                    $this->sendOrSchedule($notificationConfig, $newCommunication, $recipients);
                }
            }
        }

        return true;
    }

}
