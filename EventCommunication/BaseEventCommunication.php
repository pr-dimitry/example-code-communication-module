<?php


namespace App\Libraries\Communication\EventCommunication;

use App\Libraries\Communication\Contracts\CommunicationModelInterface;
use App\Libraries\Communication\Contracts\EventCommunicationDTOInterface;
use App\Libraries\Communication\Jobs\SendCommunication;
use App\Libraries\Communication\Models\Communication;
use App\Libraries\Communication\Services\CommunicationService;
use App\Libraries\Communication\Services\CommunicationSettingsService;
use Illuminate\Support\Collection;

/**
 * Class BaseEventCommunication
 *
 * @property CommunicationModelInterface $model
 *
 * @package App\Libraries\Communication\EventCommunication
 */
abstract class BaseEventCommunication
{

    protected $className;
    protected $trigger;
    protected $DTO;
    protected $config;
    protected $model;
    protected $user_id;
    protected $settings;
    protected $channels;
    protected $communication_service;
    protected $recipients;

    /**
     * BaseEventCommunication constructor.
     * @param string $className
     * @param string $trigger
     * @param EventCommunicationDTOInterface $DTO
     * @param array $config
     */
    public function __construct(string $className, string $trigger, EventCommunicationDTOInterface $DTO, array $config)
    {
        $this->className = $className;
        $this->trigger = $trigger;
        $this->DTO = $DTO;
        $this->config = $config;
        $this->user_id = $DTO->getUserId();
        $this->model = $this->getModel();
        $this->settings = $this->getCommunicationSettings();
        $this->channels = config('communication.channels');
        $this->communication_service = new CommunicationService();
    }

    /**
     * @return mixed
     */
    protected function getModel()
    {
        return $this->DTO->getModelId()
            ? $this->className::find($this->DTO->getModelId())
            : null;
    }

    /**
     * @return array
     */
    protected function getCommunicationSettings()
    {
        return CommunicationSettingsService::getUserSettings($this->user_id);
    }

    /**
     * @param array $channelsSettings
     * @param string $channel
     * @return bool
     */
    protected function channelAvailable(array $channelsSettings, string $channel)
    {
        $result = false;

        if (array_key_exists($channel, $channelsSettings)) {
            $result = (bool) $channelsSettings[$channel];
        }

        return $result;
    }

    /**
     * @param array $notificationConfig
     * @param Communication $communication
     * @param Collection $recipients
     */
    public function sendOrSchedule(array $notificationConfig, Communication $communication, Collection $recipients)
    {
        if ($notificationConfig['scheduled']) {
            $this->communication_service->schedule($communication);
        } else {
            foreach ($recipients as $recipient) {
                SendCommunication::dispatch($communication, $recipient);
            }
        }
    }

}
