<?php


namespace App\Libraries\Communication\EventCommunication;

use App\Libraries\Communication\Contracts\EventCommunicationDTOInterface;

class EventCommunication
{
    protected const METHOD_POSTFIX = '_communication';

    protected $className;
    protected $config;
    protected $trigger;
    protected $DTO;

    /**
     * EventCommunication constructor.
     * @param string $className
     * @param string $trigger
     * @param EventCommunicationDTOInterface $DTO
     */
    public function __construct(string $className, string $trigger, EventCommunicationDTOInterface $DTO)
    {
        $this->className = $className;
        $this->config = config("communication.event_communication.$className");
        $this->trigger = $trigger;
        $this->DTO = $DTO;
    }

    /**
     * @return mixed
     * @throws \ErrorException
     */
    public function run()
    {
        if (empty($this->config)) {
            throw new \ErrorException(__('communication.errors.invalid_config'));
        }

        $handlerClassName = $this->config['class'];
        $methodName = $this->trigger . self::METHOD_POSTFIX;
        return (new $handlerClassName($this->className, $this->trigger, $this->DTO, $this->config))->$methodName();
    }
}
