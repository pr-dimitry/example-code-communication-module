<?php


namespace App\Libraries\Communication\Traits;


use App\Libraries\Communication\Models\Communication;

trait CommunicationRecipientTrait
{

    /**
     * @return mixed
     */
    public function communications()
    {
        return $this->morphToMany(Communication::class, 'recipient', 'communication_recipients');
    }

    /**
     * @return mixed
     */
    public function getToEmail()
    {
        return $this->email;
    }

    /**
     * @return mixed
     */
    public function getPhoneNumber()
    {
        return $this->phone;
    }
}
