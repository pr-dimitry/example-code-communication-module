<?php

namespace App\Libraries\Communication;

use Illuminate\Support\ServiceProvider;

class CommunicationServiceProvider extends ServiceProvider
{

    public function boot()
    {
        \View::addNamespace('communication', __DIR__ . '/templates');
    }

	public function register()
	{
        $this->mergeConfigFrom(__DIR__ . '/config/communication.php', 'communication' );

		$this->app->bind('communication', function () {
			return new CommunicationClass();
		});
	}
}
