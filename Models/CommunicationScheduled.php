<?php

namespace App\Libraries\Communication\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Libraries\Communication\Models\CommunicationScheduled
 *
 * @property int $id
 * @property string $scheduled_time
 * @property int $communication_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Libraries\Communication\Models\CommunicationScheduled newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Libraries\Communication\Models\CommunicationScheduled newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Libraries\Communication\Models\CommunicationScheduled query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Libraries\Communication\Models\CommunicationScheduled whereCommunicationId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Libraries\Communication\Models\CommunicationScheduled whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Libraries\Communication\Models\CommunicationScheduled whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Libraries\Communication\Models\CommunicationScheduled whereScheduledTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Libraries\Communication\Models\CommunicationScheduled whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class CommunicationScheduled extends Model
{

    const QUERY_BEFORE_HOURS = 6;

    protected $table = 'communication_scheduled';

    protected $fillable = [
        'communication_id',
        'scheduled_time',
    ];

    public function communication()
    {
        $this->belongsTo(Communication::class);
    }

}
