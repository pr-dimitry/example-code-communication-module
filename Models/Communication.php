<?php

namespace App\Libraries\Communication\Models;

use App\Models\Client;
use App\Models\User;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Libraries\Communication\Models\Communication
 *
 * @property int $id
 * @property string $status
 * @property string $channel
 * @property array $data
 * @property string|null $scheduled_time
 * @property string|null $sent_at
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property mixed|null $sending_result
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Client[] $recipients_clients
 * @property-read int|null $recipients_clients_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\User[] $recipients_users
 * @property-read int|null $recipients_users_count
 * @property-read \App\Libraries\Communication\Models\CommunicationScheduled $scheduled
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Libraries\Communication\Models\Communication newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Libraries\Communication\Models\Communication newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Libraries\Communication\Models\Communication query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Libraries\Communication\Models\Communication whereChannel($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Libraries\Communication\Models\Communication whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Libraries\Communication\Models\Communication whereData($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Libraries\Communication\Models\Communication whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Libraries\Communication\Models\Communication whereScheduledTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Libraries\Communication\Models\Communication whereSendingResult($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Libraries\Communication\Models\Communication whereSentAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Libraries\Communication\Models\Communication whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Libraries\Communication\Models\Communication whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Communication extends Model
{

    const STATUS_CREATED    = 'created';
    const STATUS_SCHEDULED  = 'scheduled';
    const STATUS_SENT       = 'sent';
    const STATUS_READ       = 'read';
    const STATUS_ERROR      = 'error';

    protected $fillable = [
        'status',
        'channel',
        'data',
        'scheduled_time',
        'sent_at',
        'read_at',
        'sending_result',
    ];

    protected $casts = [
        'data' => 'array',
        'sending_result' => 'array'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphToMany
     */
    public function recipients_users()
    {
        return $this->morphedByMany(
            User::class,
            'recipient',
            'communication_recipients'
        );
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphToMany
     */
    public function recipients_clients()
    {
        return $this->morphedByMany(
            Client::class,
            'recipient',
            'communication_recipients'
        );
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function scheduled()
    {
        return $this->hasOne(CommunicationScheduled::class);
    }


    /**
     * @return \Illuminate\Support\Collection
     */
    public function getAllRecipients()
    {
        $recipients = collect([]);

        $users = $this->recipients_users;
        $clients = $this->recipients_clients;

        $recipients->push($users);
        $recipients->push($clients);

        return $recipients->flatten();
    }

}
