<?php

namespace App\Libraries\Communication\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Libraries\Communication\Models\CommunicationSettings
 *
 * @property int $id
 * @property int $user_id
 * @property array $settings_data
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Libraries\Communication\Models\CommunicationSettings newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Libraries\Communication\Models\CommunicationSettings newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Libraries\Communication\Models\CommunicationSettings query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Libraries\Communication\Models\CommunicationSettings whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Libraries\Communication\Models\CommunicationSettings whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Libraries\Communication\Models\CommunicationSettings whereSettingsData($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Libraries\Communication\Models\CommunicationSettings whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Libraries\Communication\Models\CommunicationSettings whereUserId($value)
 * @mixin \Eloquent
 */
class CommunicationSettings extends Model
{

    protected $casts = [
        'settings_data' => 'array'
    ];

    protected $fillable = [
        'user_id',
        'settings_data',
    ];


}
