<?php


namespace App\Libraries\Communication\ChannelsServices\Clients\Sms;


use App\Libraries\Communication\Exceptions\AuthException;

class SmsSoapClient
{

    protected $client;

    /**
     * SmsSoapClient constructor.
     * @throws AuthException
     * @throws \SoapFault
     */
    public function __construct()
    {
        $this->client = new \SoapClient(config('sms.soap_endpoint'));
        $this->auth();
    }

    /**
     * @throws AuthException
     */
    protected function auth()
    {
        $this->client->Auth([
            'login' => config('sms.login'),
            'password' => config('sms.password')
        ]);

        $response = $this->client->GetCreditBalance();
        if (!property_exists($response, 'GetCreditBalanceResult')) {
            throw new AuthException(__('communication.errors.sms_auth'));
        }
    }

    /**
     * @param string $phoneNumber
     * @param $messageText
     * @return mixed
     */
    public function send(string $phoneNumber, $messageText)
    {
        $sms = [
            'sender' => config('sms.sender'),
            'destination' => $phoneNumber,
            'text' => $messageText
        ];
        return $this->client->SendSMS($sms);
    }

}
