<?php


namespace App\Libraries\Communication\ChannelsServices;


use App\Libraries\Communication\Contracts\ChannelServiceStrategyInterface;
use App\Libraries\Communication\DTO\EmailChannelDTO;
use App\Libraries\Communication\Models\Communication;
use App\Libraries\Communication\Services\CommunicationService;
use Illuminate\Support\Facades\Log;

/**
 * Class Email
 *
 * @property EmailChannelDTO $DTO
 * @package App\Libraries\Communication\ChannelsServices
 */
class Email implements ChannelServiceStrategyInterface
{

    protected $DTO;
    protected $communication;

    /**
     * Email constructor.
     * @param Communication $communication
     * @param $DTO
     */
    public function __construct(Communication $communication, $DTO)
    {
        $this->DTO = $DTO;
        $this->communication = $communication;
    }

    /**
     * @param array|null $config
     * @return mixed|void
     * @throws \Exception
     */
    public function send(array $config = null)
    {
        \App::environment('local')
            ? $this->sendToLog()
            : $this->sendWithMailer();

        // delete from communication_scheduled table
        CommunicationService::deleteScheduled($this->communication);

        CommunicationService::changeStatus($this->communication, Communication::STATUS_SENT);
    }

    /**
     * Send email using Laravel Mailer
     */
    protected function sendWithMailer()
    {

        \Mail::send(
            ['html' => null, 'text' => null],
            [],
            function ($message) {
                $message->from(
                    $this->DTO->getFromEmail(),
                    $this->DTO->getFromName()
                );
                $message->to($this->DTO->getTo());
                if($this->DTO->getSubject()) {$message->subject($this->DTO->getSubject());}
                $message->setBody($this->DTO->getBody(), 'text/html');
            }
        );

        Log::info(__('communication.messages.successfully_sent'));
        return true;
    }

    /**
     * Logging messages
     */
    public function sendToLog()
    {
        Log::channel('emails_sent')->info(__('communication.messages.successfully_log_sent', [
            'subject' => $this->DTO->getSubject(),
            'body' => $this->DTO->getBody()
        ]));
        return true;
    }

}
