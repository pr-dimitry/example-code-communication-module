<?php


namespace App\Libraries\Communication\ChannelsServices;


use App\Libraries\Communication\ChannelsServices\Clients\Sms\SmsSoapClient;
use App\Libraries\Communication\Contracts\ChannelServiceStrategyInterface;
use App\Libraries\Communication\DTO\SmsChannelDTO;
use App\Libraries\Communication\Models\Communication;
use App\Libraries\Communication\Services\CommunicationService;
use Illuminate\Support\Facades\Log;

/**
 * Class Sms
 *
 * @property SmsChannelDTO $DTO
 * @package App\Libraries\Communication\ChannelsServices
 */
class Sms implements ChannelServiceStrategyInterface
{

    protected $DTO;
    protected $communication;

    /**
     * Sms constructor.
     * @param Communication $communication
     * @param $DTO
     */
    public function __construct(Communication $communication, $DTO)
    {
        $this->DTO = $DTO;
        $this->communication = $communication;
    }

    /**
     * @param array|null $config
     * @return mixed|void
     * @throws \Exception
     */
    public function send(array $config = null)
    {
        \App::environment('local')
            ? $this->sendToLog()
            : $this->sendWithMailer();

        $this->sendWithMailer();

        // delete from communication_scheduled table
        CommunicationService::deleteScheduled($this->communication);
    }

    /**
     * @throws \App\Libraries\Communication\Exceptions\AuthException
     * @throws \SoapFault
     */
    protected function sendWithClient()
    {
        $client = new SmsSoapClient();
        $response = $client->send($this->validatedNumber(), $this->DTO->getText());

        if (property_exists($response, 'SendSMSResult')) {
            CommunicationService::changeStatus($this->communication, Communication::STATUS_SENT, $response->SendSMSResult->ResultArray);
        } else {
            CommunicationService::changeStatus($this->communication, Communication::STATUS_ERROR);
        }
        $this->communication->save();
    }

    /**
     * Logging messages
     */
    public function sendToLog()
    {
        Log::channel('sms_sent')->info(__('communication.messages.successfully_log_sent', [
            'subject' => '<none>',
            'body' => $this->DTO->getText()
        ]));
        CommunicationService::changeStatus($this->communication, Communication::STATUS_SENT);
    }

    /**
     * @return string
     */
    protected function validatedNumber()
    {
        return phone($this->DTO->getPhoneNumber(), config('sms.country'))->formatE164();
    }
}
