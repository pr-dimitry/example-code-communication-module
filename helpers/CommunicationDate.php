<?php


namespace App\Libraries\Communication\helpers;


use App\Libraries\Communication\Contracts\CommunicationModelInterface;
use Carbon\Carbon;

class CommunicationDate
{

    // this will be a parts of Carbon methods signature, e.g. subDays()
    const SCHEDULE_BEFORE = 'sub';
    const SCHEDULE_AFTER = 'add';

    const SCHEDULE_HOURS = 'hours';
    const SCHEDULE_DAYS = 'days';
    const SCHEDULE_WEEKS = 'weeks';

    public static function setScheduledDate(array &$notificationConfig, CommunicationModelInterface $model)
    {
        $baseDateTime = $model[$notificationConfig['schedule_attribute']];
        $methodName = $notificationConfig['schedule_category'] . ucfirst($notificationConfig['scheduled_measure']);
        $resultDateTime = Carbon::parse($baseDateTime)->$methodName((int)$notificationConfig['scheduled_value'])->format('Y-m-d H:i:s');
        $notificationConfig['scheduled_date_time'] = $resultDateTime;
    }

}
