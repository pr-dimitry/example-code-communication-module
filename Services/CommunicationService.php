<?php

namespace App\Libraries\Communication\Services;

use App\Libraries\Communication\Models\Communication;
use App\Libraries\Communication\Models\CommunicationScheduled;
use Carbon\Carbon;

class CommunicationService
{

    /**
     * @param array $message
     * @param string $channel
     * @param array $notificationConfig
     * @return Communication
     */
    public function createCommunication(array $message, string $channel, array $notificationConfig)
    {
        $communication = new Communication();

        $communication->status = $notificationConfig['scheduled']
            ? Communication::STATUS_SCHEDULED
            : Communication::STATUS_CREATED;

        $communication->channel = $channel;
        $communication->data = $message;

        $communication->scheduled_time = $notificationConfig['scheduled']
            ? $notificationConfig['scheduled_date_time']
            : null;

        $communication->save();

        return $communication;
    }


    /**
     * @param string $notification
     * @return array|\Illuminate\Contracts\Translation\Translator|string|null
     */
    public function getMessageData(string $type, string $notification, $model = null)
    {
        $heading = $this->getMessageHeading($notification);
        $text = $this->getMessageText($type, $notification, $model);
        return compact('heading', 'text');
    }

    /**
     * @param string $notification
     * @return array|\Illuminate\Contracts\Translation\Translator|string|null
     */
    protected function getMessageHeading(string $notification)
    {
        return __("communication.event_communication.$notification.message_text");
    }


    protected function getMessageText(string $type, string $notification, $model = null)
    {
        $methodName = 'getMessageText' . ucfirst($type);
        return $this->$methodName($notification, $model);
    }

    /**
     * @param string $notification
     * @return array|\Illuminate\Contracts\Translation\Translator|string|null
     */
    protected function getMessageTextRaw(string $notification)
    {
        return __("communication.event_communication.$notification.message_text");
    }

    /**
     * @param string $notification
     * @param null $model
     * @return array|string
     * @throws \Throwable
     */
    protected function getMessageTextHtml(string $notification, $model = null)
    {
        return view('communication::' . $notification, ['model' => $model])->render();
    }

    /**
     * @param Communication $communication
     */
    public function schedule(Communication $communication)
    {
        $scheduledCommunication = new CommunicationScheduled();
        $scheduledCommunication->communication_id = $communication->id;
        $scheduledCommunication->scheduled_time = $communication->scheduled_time;
        $scheduledCommunication->save();
    }

    /**
     * @param int $beforeHours
     * @return Communication[]|\Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection
     */
    public function getScheduled($beforeHours = CommunicationScheduled::QUERY_BEFORE_HOURS)
    {

        return Communication::query()
            ->whereHas('scheduled', function ($query) use ($beforeHours) {
                $query->whereBetween('scheduled_time', [
                    Carbon::now()->subHours($beforeHours)->format('Y-m-d H:i:s'),
                    Carbon::now()->format('Y-m-d H:i:s'),
                ]);
            })
            ->get();
    }

    /**
     * @param Communication $communication
     * @throws \Exception
     */
    public static function deleteScheduled(Communication $communication)
    {
        if ($communication->scheduled) {
            $communication->scheduled->delete();
        }
    }

    /**
     * @param Communication $communication
     * @param string $status
     * @param array $sendingResult
     */
    public static function changeStatus(Communication $communication, string $status, array $sendingResult = null)
    {
        if ($sendingResult) {
            $communication->sending_result = $sendingResult;
        }
        $communication->status = $status;
        $communication->save();
    }



}
