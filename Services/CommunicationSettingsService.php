<?php

namespace App\Libraries\Communication\Services;


use App\Libraries\Communication\Models\CommunicationSettings;

class CommunicationSettingsService
{
    /**
     * @param int $user_id
     * @return array|mixed
     */
    public static function getUserSettings(int $user_id)
    {
        $settings = CommunicationSettings::query()
            ->where('user_id', $user_id)
            ->first();

        return $settings
            ? $settings->settings_data
            : [];
    }
}
